import { Moment } from 'moment';

export const enum TodoStatus {
  PENDING = 'PENDING',
  IN_PROGRESS = 'IN_PROGRESS',
  DONE = 'DONE'
}

export interface ITodo {
  id?: number;
  name?: string;
  description?: string;
  status?: TodoStatus;
  createdAt?: Moment;
}

export const defaultValue: Readonly<ITodo> = {};
