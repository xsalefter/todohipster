package com.todohipster.domain.enumeration;

/**
 * The TodoStatus enumeration.
 */
public enum TodoStatus {
    PENDING, IN_PROGRESS, DONE
}
