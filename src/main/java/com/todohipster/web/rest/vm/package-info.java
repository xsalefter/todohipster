/**
 * View Models used by Spring MVC REST controllers.
 */
package com.todohipster.web.rest.vm;
