/* tslint:disable no-unused-expression */
import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import TodoComponentsPage from './todo.page-object';
import { TodoDeleteDialog } from './todo.page-object';
import TodoUpdatePage from './todo-update.page-object';
import { waitUntilDisplayed, waitUntilHidden } from '../../util/utils';

const expect = chai.expect;

describe('Todo e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let todoUpdatePage: TodoUpdatePage;
  let todoComponentsPage: TodoComponentsPage;
  let todoDeleteDialog: TodoDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();

    await waitUntilDisplayed(navBarPage.entityMenu);
  });

  it('should load Todos', async () => {
    await navBarPage.getEntityPage('todo');
    todoComponentsPage = new TodoComponentsPage();
    expect(await todoComponentsPage.getTitle().getText()).to.match(/Todos/);
  });

  it('should load create Todo page', async () => {
    await todoComponentsPage.clickOnCreateButton();
    todoUpdatePage = new TodoUpdatePage();
    expect(await todoUpdatePage.getPageTitle().getText()).to.match(/Create or edit a Todo/);
  });

  it('should create and save Todos', async () => {
    const nbButtonsBeforeCreate = await todoComponentsPage.countDeleteButtons();

    await todoUpdatePage.setNameInput('name');
    expect(await todoUpdatePage.getNameInput()).to.match(/name/);
    await todoUpdatePage.setDescriptionInput('description');
    expect(await todoUpdatePage.getDescriptionInput()).to.match(/description/);
    await todoUpdatePage.statusSelectLastOption();
    await todoUpdatePage.setCreatedAtInput('01-01-2001');
    expect(await todoUpdatePage.getCreatedAtInput()).to.eq('2001-01-01');
    await waitUntilDisplayed(todoUpdatePage.getSaveButton());
    await todoUpdatePage.save();
    await waitUntilHidden(todoUpdatePage.getSaveButton());
    expect(await todoUpdatePage.getSaveButton().isPresent()).to.be.false;

    await todoComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeCreate + 1);
    expect(await todoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
  });

  it('should delete last Todo', async () => {
    await todoComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeDelete = await todoComponentsPage.countDeleteButtons();
    await todoComponentsPage.clickOnLastDeleteButton();

    const deleteModal = element(by.className('modal'));
    await waitUntilDisplayed(deleteModal);

    todoDeleteDialog = new TodoDeleteDialog();
    expect(await todoDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/todohipsterApp.todo.delete.question/);
    await todoDeleteDialog.clickOnConfirmButton();

    await todoComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
    expect(await todoComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
